-- --------------------------------------------------------
/*
--
-- Cấu trúc bảng cho bảng `album`
--

DROP TABLE IF EXISTS ``;
CREATE TABLE `album` (
  `id` 	bigint(20) UNSIGNED NOT NULL,
  `album_code` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `album_name` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
   `description` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
    `created_at` datetime COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE= utf8_general_ci;
*/
--
-- Đang đổ dữ liệu cho bảng `album`
--
INSERT INTO `album` (`id`, `album_code`, `album_name`, `created_at`, `description`) VALUES ('1', '123', 'album01', '2023-01-08 23:20:39', 'album 01 ');
INSERT INTO `album` (`id`, `album_code`, `album_name`, `created_at`, `description`) VALUES ('2', '456', 'album02', '2023-01-08 23:20:39', 'album 02 ');
INSERT INTO `album` (`id`, `album_code`, `album_name`, `created_at`, `description`) VALUES ('3', '789', 'album03', '2023-01-08 23:20:39', 'album 03 ');

-- --------------------------------------------------------
/*
--
-- Cấu trúc bảng cho bảng `photo`
--

DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `id` 	bigint(20) UNSIGNED NOT NULL,
  `photo_code` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `photo_name` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `photo_link` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
   `photo_description` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
    `created_at` datetime COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
*/
--
-- Đang đổ dữ liệu cho bảng `district`
--
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('1', '2023-01-08 23:20:39', 'PT123', 'photo description', 'photo link', 'photo01', '1');
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('2', '2023-01-08 23:20:39', 'PT456', 'photo description', 'photo link', 'photo02', '1');
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('3', '2023-01-08 23:20:39', 'PT789', 'photo description', 'photo link', 'photo03', '2');
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('4', '2023-01-08 23:20:39', 'PT185', 'photo description', 'photo link', 'photo04', '2');
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('5', '2023-01-08 23:20:39', 'PT782', 'photo description', 'photo link', 'photo05', '3');
INSERT INTO `photo` (`id`, `created_at`, `photo_code`, `photo_description`, `photo_link`, `photo_name`, `album_id`) VALUES ('6', '2023-01-08 23:20:39', 'PT092', 'photo description', 'photo link', 'photo06', '3');