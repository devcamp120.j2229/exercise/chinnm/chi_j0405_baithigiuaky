package com.backend.restapi.respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backend.restapi.model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long> {
    Optional<Album> findByAlbumCode(String albumCode);
}
