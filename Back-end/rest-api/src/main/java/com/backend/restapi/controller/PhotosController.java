package com.backend.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.restapi.model.Album;
import com.backend.restapi.model.Photo;
import com.backend.restapi.respository.AlbumRepository;
import com.backend.restapi.respository.PhotoRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class PhotosController {
    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private PhotoRepository photoRepository;

    @GetMapping("/album/{albumId}/photos")
    public ResponseEntity<Object> getPhotoByAlbumId(@PathVariable(value = "albumId", required = true) Long albumId) {
        try {
            List<Photo> photo = photoRepository.findByAlbumId(albumId);
            if (!photo.isEmpty()) {
                return new ResponseEntity<>(photo, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/photos/all")
    public ResponseEntity<Object> getAllPhoto() {
        try {
            List<Photo> photoList = new ArrayList<Photo>();
            photoRepository.findAll().forEach(photoEmlement -> {
                photoList.add(photoEmlement);
            });
            if (!photoList.isEmpty()) {
                return new ResponseEntity<>(photoList, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/photos/detail/{id}")
    public ResponseEntity<Object> getPhotoById(@PathVariable(name = "id", required = true) Long id) {

        try {
            Optional<Photo> photo = photoRepository.findById(id);
            if (photo.isPresent()) {
                return new ResponseEntity<>(photo, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/albums/detail_by_photocode/{photoCode}")
    public ResponseEntity<Object> getPhotoByPhotoCode(
            @PathVariable(name = "photoCode", required = true) String photoCode) {

        try {
            Optional<Photo> photo = photoRepository.findByPhotoCode(photoCode);
            if (photo.isPresent()) {
                return new ResponseEntity<>(photo, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/photo/create/{id}")
    public ResponseEntity<Object> createPhoto(@PathVariable(name = "id", required = true) Long id,
            @Valid @RequestBody Photo photo) {
        try {
            Optional<Album> albumData = albumRepository.findById(id);
            if (albumData.isPresent()) {
                Optional<Photo> photoData = photoRepository.findByPhotoCode(photo.getPhotoCode());
                if (!photoData.isPresent()) {
                    Photo newPhoto = new Photo();
                    newPhoto.setPhotoCode(photo.getPhotoCode());
                    newPhoto.setPhotoName(photo.getPhotoName());
                    newPhoto.setPhotoDescription(photo.getPhotoDescription());
                    newPhoto.setPhotoLink(photo.getPhotoLink());
                    newPhoto.setCreated(new Date());

                    Album album = albumData.get();
                    newPhoto.setAlbum(album);

                    Photo savePhoto = photoRepository.save(newPhoto);
                    return new ResponseEntity<>(savePhoto, HttpStatus.CREATED);

                } else {
                    return ResponseEntity.badRequest().body("Photo already exists");
                }

            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Photo: " + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/photo/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "id") Long id, @RequestBody Photo photoUpdate) {

        try {
            Optional<Photo> photoData = photoRepository.findById(id);
            if (photoData.isPresent()) {
                Photo photo = photoData.get();
                photo.setPhotoCode(photoUpdate.getPhotoCode());
                photo.setPhotoDescription(photoUpdate.getPhotoDescription());
                photo.setPhotoLink(photoUpdate.getPhotoLink());
                photo.setPhotoName(photoUpdate.getPhotoName());

                return ResponseEntity.ok(photoRepository.save(photo));
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation about this entity" + e.getCause().getCause().getMessage());
        }

    }

    @DeleteMapping("/photo/delete/{id}")
    public ResponseEntity<Object> deletePhotoById(@PathVariable(name = "id", required = true) Long id) {
        try {
            Optional<Photo> deletePhoto = photoRepository.findById(id);
            if (deletePhoto.isPresent()) {
                photoRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
