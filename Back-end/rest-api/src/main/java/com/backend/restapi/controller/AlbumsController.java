package com.backend.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.restapi.model.Album;
import com.backend.restapi.respository.AlbumRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class AlbumsController {
    @Autowired
    AlbumRepository albumRepository;

    @GetMapping("/albums/all")
    public ResponseEntity<Object> getAllAlbum() {
        try {
            List<Album> albumList = new ArrayList<Album>();
            albumRepository.findAll().forEach(albumEmlement -> {
                albumList.add(albumEmlement);
            });
            if (!albumList.isEmpty()) {
                return new ResponseEntity<>(albumList, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/albums/detail_by_id/{id}")
    public ResponseEntity<Object> getAlbumById(@PathVariable(name = "id", required = true) Long id) {

        try {
            Optional<Album> album = albumRepository.findById(id);
            if (album.isPresent()) {
                return new ResponseEntity<>(album, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/albums/detail_by_albumcode/{albumCode}")
    public ResponseEntity<Object> getAlbumByAlbumCode(
            @PathVariable(name = "albumCode", required = true) String albumCode) {

        try {
            Optional<Album> album = albumRepository.findByAlbumCode(albumCode);
            if (album.isPresent()) {
                return new ResponseEntity<>(album, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/albums")
    public ResponseEntity<Object> createAlbum(@Valid @RequestBody Album album) {
        try {
            Optional<Album> albumFound = albumRepository.findByAlbumCode(album.getAlbumCode());
            if (albumFound.isPresent()) {
                return ResponseEntity.badRequest().body("Album already exists");
            } else {
                Album newAlbum = new Album();
                newAlbum.setAlbumCode(album.getAlbumCode());
                newAlbum.setAlbumName(album.getAlbumName());
                newAlbum.setDescription(album.getDescription());
                newAlbum.setCreated(new Date());

                Album saveAlbum = albumRepository.save(newAlbum);
                return new ResponseEntity<>(saveAlbum, HttpStatus.CREATED);

            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Album: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/album/update/{id}")
    public ResponseEntity<Object> updateAlbum(@PathVariable(name = "id", required = true) Long id,
            @RequestBody Album album) {

        try {
            Optional<Album> albumData = albumRepository.findById(id);
            if (albumData.isPresent()) {
                Album albumUpdate = albumData.get();
                albumUpdate.setAlbumCode(album.getAlbumCode());
                albumUpdate.setAlbumName(album.getAlbumName());
                albumUpdate.setDescription(album.getDescription());
                return new ResponseEntity<>(albumRepository.save(albumUpdate), HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest().body("Failed to get specified ALbum: " + id + "  for update.");
            }

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Album:" + e.getCause().getCause().getMessage());
        }

    }

    @CrossOrigin
    @DeleteMapping("/album/delete/{id}")
    public ResponseEntity<Object> deleteAlbumById(@PathVariable(name = "id", required = true) Long id) {
        try {
            Optional<Album> deleteAlbum = albumRepository.findById(id);
            if (deleteAlbum.isPresent()) {
                albumRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
